DROP DATABASE IF EXISTS modcadastro;


CREATE DATABASE modcadastro;

\c modcadastro;

/*
CREATE TABLE curriculos (
  ID SERIAL PRIMARY KEY,
  name VARCHAR[500],
  email VARCHAR[500],
  telefone VARCHAR[11],
  apresentacao VARCHAR[50000],
  linkedin VARCHAR[500],
  github VARCHAR[500],
  nivel_ingles VARCHAR[20],
  pretensao_salarial REAL,
  caminho_curriculo VARCHAR[500],
  create_at TIMESTAMP
);

CREATE TABLE users (
  ID SERIAL PRIMARY KEY,
  name VARCHAR[500],
  email VARCHAR[500],
  email_verified_at TIMESTAMP,
  password VARCHAR[500],
  remember_token VARCHAR[500],
  create_at TIMESTAMP
);

CREATE TABLE passwords (
  email VARCHAR[500],
  token VARCHAR[500],
  create_at TIMESTAMP
);

CREATE TABLE vagas (
  ID SERIAL PRIMARY KEY,
  title VARCHAR[500],
  locale VARCHAR[500],
  description VARCHAR[500],
  create_at TIMESTAMP
);
*/
/*INSERT INTO curriculos (name, email, telefone, apresentacao, linkedin, github, nivel_ingles, pretensao_salarial, caminho_curriculo, create_at)
  VALUES ('Candidato1', 'cd1@cd1.com', '84988881111', 'Desenvolvedor de Sistemas', 'linkedin.com', 'github.com', 'avançado', '4000', '', CURRENT_TIMESTAMP);

INSERT INTO user (name, email, email_verified_at, password, remember_token , create_at)
  VALUES ('Administrador', 'adm@adm.com', null, '', 'token', CURRENT_TIMESTAMP);




CREATE TABLE teste.modcadastro (
  ID SERIAL PRIMARY KEY,
  name VARCHAR,
  registry VARCHAR,
  affiliation VARCHAR,
  launched INTEGER,
  class VARCHAR,
  captain VARCHAR
);


INSERT INTO teste.modcadastro (name, registry, affiliation, launched, class, captain)
  VALUES ('USS Enterprise', 'NCC-1701', 'United Federation of Planets Starfleet', 2258, 'Constitution', 'James T. Kirk');

*/

/*HEROKU*/

/*
CREATE ROLE modulo;
ALTER ROLE modulo WITH LOGIN PASSWORD '123' NOSUPERUSER NOCREATEDB NOCREATEROLE;
CREATE DATABASE modcadastro OWNER modulo;
REVOKE ALL ON DATABASE modcadastro FROM PUBLIC;
GRANT CONNECT ON DATABASE modcadastro TO modulo;
GRANT ALL ON DATABASE modcadastro TO modulo;
GRANT ALL ON modcadastro_id_seq TO modulo;*/
