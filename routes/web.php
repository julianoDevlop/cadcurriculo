<?php

use App\Vaga;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//$vagas = Vaga::orderBy('created_at', 'asc')->get();        
//return view('home', ['vagas' => $vagas]);

Route::get('/', function () {
    $vagas = Vaga::paginate(5);
    return view('welcome',['vagas' => $vagas]);
});

Auth::routes();// vendor/laravel/framework/src/Illuminate/Auth

Route::get('/home', 'HomeController@index')->name('home');
//Route::get('/curriculo', 'CurriculoController@index')->name('curriculo');
Route::resource('curriculo','CurriculoController');
/*Route::get('/curriculo', function()
{
    return View::make('members/login');
});
*/
Route::post('curriculo{id}', 'CurriculoController@store');
Route::get('curriculo{vaga}', 'CurriculoController@index');
Route::post('vaga', 'VagaController@store');
Route::resource('vaga','VagaController');

/* Este comentário serve apenas como guia do Auth::routes() e seu entendimento.
// Authentication Routes...
$this->get('login', 'Auth\LoginController@showLoginForm')->name('login');
$this->post('login', 'Auth\LoginController@login');
$this->post('logout', 'Auth\LoginController@logout')->name('logout');

// Registration Routes...
$this->get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
$this->post('register', 'Auth\RegisterController@register');

// Password Reset Routes...
$this->get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
$this->post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
$this->get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
$this->post('password/reset', 'Auth\ResetPasswordController@reset');
*/

