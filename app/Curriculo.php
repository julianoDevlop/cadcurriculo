<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Curriculo extends Model
{
    /**
     * Relacionamento OneToMay (inverso) com vagas
     * 
     * @return App\Vaga
     */
     public function vagas()
     {
         return $this->belongsTo('App\Vaga');
     }
}
