<?php

namespace App\Http\Controllers;

use App\Curriculo;
use App\Vaga;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL;

class CurriculoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Vaga $vaga)
    {
        //return view('curriculo',['vaga' => $vaga]);
        return view('curriculo',compact('vaga'));        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
        /*return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
        ]);*/
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $vaga_id)
    {
        //dd($request);
        
        $nome_arquivo1 = str_replace("@","AT",$request->email);
        $nome_arquivo = str_replace(".","DOT",$nome_arquivo1);
        $caminho_arquivo = $request->file('arquivo')->store('curriculos/'.$nome_arquivo);

        $curriculo = new Curriculo;
        $curriculo->vaga_id = $vaga_id;
        $curriculo->name = $request->name;
        $curriculo->email = $request->email;
        $curriculo->telefone = $request->telefone;
        $curriculo->apresentacao = $request->apres;
        $curriculo->linkedin = $request->linkedin;
        $curriculo->github = $request->github;
        $curriculo->nivel_ingles = $request->ingles;
        $curriculo->pretensao_salarial = $request->sal;
        $curriculo->caminho_curriculo = $caminho_arquivo;
        $curriculo->status = "";

        $curriculo->save();


        

        //return $caminho_arquivo;

        /*$messages = [
    		'required' => 'Por favor, selecione um arquivo para enviar',
		];
 
    	$this->validate($request, [
    		'file' => 'required',
    	], $messages);*/

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Curriculo  $curriculo
     * @return \Illuminate\Http\Response
     */
    public function show(Curriculo $curriculo)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Curriculo  $curriculo
     * @return \Illuminate\Http\Response
     */
    public function edit(Curriculo $curriculo)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Curriculo  $curriculo
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Curriculo $curriculo)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Curriculo  $curriculo
     * @return \Illuminate\Http\Response
     */
    public function destroy(Curriculo $curriculo)
    {
        //
    }
}
