<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Vaga;
use App\Curriculo;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $vagas = Vaga::paginate(5);//orderBy('created_at', 'asc')->get();
        $curriculos = Curriculo::paginate(5);
        echo $curriculos;
        return view('home',['vagas' => $vagas,'curriculos' => $curriculos]);
    }

    
}
