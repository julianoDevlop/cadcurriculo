<?php

namespace App\Http\Controllers;

use App\Vaga;
use App\Curriculo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

class VagaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $vagas = Vaga::orderBy('created_at', 'asc')->get();        
        return view('home', ['vagas' => $vagas]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        // store
        $vaga = new Vaga;
        $vaga->user_id = $id = Auth::id();//ID do usuário
        $vaga->title = $request->titulo;
        $vaga->locale = $request->local;
        $vaga->description = $request->desc;

        $vaga->save();

        $v = Vaga::paginate(5);//orderBy('created_at', 'asc')->get();
        $curriculos = Curriculo::paginate(5);
        
        // redirect
        //Session::flash('message', 'Successfully created nerd!');
        //return Redirect::to('home')->with($vaga);
        echo $curriculos;
        return view('home', ['vagas' => $v, 'curriculos' => $curriculos]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Vaga  $vaga
     * @return \Illuminate\Http\Response
     */
    public function show(Vaga $vaga)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Vaga  $vaga
     * @return \Illuminate\Http\Response
     */
    public function edit(Vaga $vaga)
    {
        return view('editar',compact('vaga','vaga->id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Vaga  $vaga
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Vaga $vaga)
    {
        $vaga->title = $request->titulo;
        $vaga->locale = $request->local;
        $vaga->description = $request->desc;
        $vaga->save();
        //return redirect('v');
        $v = Vaga::paginate(5);
        $c = Curriculo::paginate(5);
        
        return view('home', ['vagas' => $v, 'curriculos' => $c]);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Vaga  $vaga
     * @return \Illuminate\Http\Response
     */
    //public function destroy(Vaga $vaga)
    public function destroy(Vaga $vaga)
    {
        //$vaga = \App\Vaga::find($id);
        $vaga->delete();
        return redirect( '/' );
        //return Redirect::to('home');
    }
}
