<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Obtém curriculos para os usuários
     *
     * @return App\Curriculo
     */
     public function curriculos()
     {
         return $this->hasManyThrough('App\Curriculo', 'App\Vaga');
     }

     /**
     * Relacionamento OneToMay com vagas
     * 
     * @return App\Vaga
     */
     public function vagas()
     {
         return $this->hasMany('App\Vaga');
     }

}
