<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vaga extends Model
{
   /**
     * Relacionamento OneToMay (inverso) com users
     * 
     * @return App\User
     */
     public function users()
     {
         return $this->belongsTo('App\User');
     }

     /**
     * Relacionamento OneToMay com curriculos
     * 
     * @return App\Curriculo
     */
     public function curriculos()
     {
         return $this->hasMany('App\Curriculo');
     }
}
