<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCurriculosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('curriculos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('vaga_id')->unsigned();
            $table->foreign('vaga_id')->references('id')->on('vagas')
                                        ->onDelete('cascade')->onUpdate('cascade');
            $table->string('name');
            $table->string('email');
            $table->string('telefone');
            $table->string('apresentacao',5000)->nullable();
            $table->string('linkedin');
            $table->string('github');
            $table->string('status');
            $table->string('nivel_ingles',20);
            $table->string('pretensao_salarial');
            $table->string('caminho_curriculo',500);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('curriculos');
    }

    
}
