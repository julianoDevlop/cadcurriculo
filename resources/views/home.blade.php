@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card" style="margin-bottom: 20px;">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    Seja bem vindo!
                </div>
                

            </div>
            <div class="card" style="margin-bottom: 20px;">
                <div class="card-header">Currículos</div>
                <div class="panel-heading">
                                    
                                </div>
                    <form class="form-vertical">
                        @if(!empty($curriculos)) 
                            <div class="panel panel-default">
                                <div class="panel-body table-responsive">
                                    <table class="table table-striped  table-hover">
                                        <thead class="thead-dark">
                                            <th>Nome</th>
                                            <th>Email</th>
                                            <th>Linkedin</th>
                                            <th>Status</th>
                                            <th>#</th>
                                        </thead>
                                        <tbody>
                                            @foreach ($curriculos as $curriculo)
                                                
                                                <tr>
                                                    <td class="table-text">
                                                        <div>{{ $curriculo->name }}</div>
                                                    </td>
                                                    <td class="table-text">
                                                        <div>{{ $curriculo->email }}</div>
                                                    </td>
                                                    <td class="table-text">
                                                        <div>{{ $curriculo->linkedin }}</div>
                                                    </td>
                                                    <td class="table-text">
                                                        <div>{{ $curriculo->status }}</div>
                                                    </td>

                                                </tr>
                                            @endforeach
                                            {{ $curriculos->links() }}
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        @else
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                Não há currículos cadastrados.
                            </div>
                        </div>

                        @endif
                    </form>
                </div>
            </div>            
            <div class="card" style="margin-bottom: 20px;">
                <div class="card-header">Vagas</div>
                    <form class="form-vertical">
                        @if(!empty($vagas)) 
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    
                                </div>

                                <div class="panel-body table-responsive">
                                    <table class="table table-striped table-hover">
                                        <thead class="thead-dark">
                                            <th>Nome</th>
                                            <th>Descrição</th>
                                            <th>Localização</th>
                                            <th></th>
                                            <th></th>
                                        </thead>

                                        <tbody>
                                            @foreach ($vagas as $vaga)
                                                <tr>
                                                    <td class="table-text">
                                                        <div>{{ $vaga->title }}</div>
                                                    </td>
                                                    <td class="table-text">
                                                        <div>{{ $vaga->description }}</div>
                                                    </td>
                                                    <td class="table-text">
                                                        <div>{{ $vaga->locale }}</div>
                                                    </td>

                                                    <td>
                                                        <a href="{{action('VagaController@edit', $vaga)}}" class="btn btn-warning">Editar</a>   
                                                    </td>

                                                    <td>
                                                        <form action="{{action('VagaController@destroy', $vaga)}}" method="post">
                                                            {{csrf_field()}}
                                                            <input type="hidden" name="_method" value="DELETE">
                                                            <button type="submit" class="btn btn-danger">Remover</button>
                                                        </form>
                                                    </td>
                                                </tr>
                                            @endforeach
                                            {{ $vagas->links() }}
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        @else
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    Não há vagas cadastradas.
                                </div>
                            </div>
                        @endif
                    </form>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="card" style="margin-bottom: 20px;">
                <div class="card-header">Vagas</div>
                <div class="panel-heading"></div>
                <form action="/vaga" method="POST" class="form-vertical">
                    {{ csrf_field() }}

                    <div class="form-group">
                        <label for="titulo" class="col-sm-3 control-label">Título</label>
                        <div class="col-sm-6">
                            <input type="text" name="titulo" id="titulo" class="form-control">
                        </div>

                        <label for="local" class="col-sm-3 control-label">Localização</label>
                        <div class="col-sm-6">
                            <input type="text" name="local" id="local" class="form-control">
                        </div>

                        <label for="desc" class="col-sm-3 control-label">Descrição</label>
                        <div class="col-sm-6">
                            <textarea type="text" name="desc" id="desc" class="form-control"></textarea>
                        </div>

                    </div>



                    <div class="form-group">
                        <div class="col-sm-offset-3 col-sm-6">
                            <button type="submit" class="btn btn-default btn-primary">
                                <i class="fa fa-plus"></i> Adicionar Vaga
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
