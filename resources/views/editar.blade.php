@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Editar</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                </div>
                

            </div>
            
            <form action="{{action('VagaController@update', $vaga)}}" method="POST" class="form-vertical">
                {{ csrf_field() }}
                <input name="_method" type="hidden" value="PATCH">

                <div class="form-group">
                    <label for="titulo" class="col-sm-6 control-label">Título</label>
                    <div id="titulo" class="col-sm-6">
                        <input type="text" name="titulo" class="form-control" value="{{$vaga->title}}">
                    </div>

                    <label for="local" class="col-sm-6 control-label">Localização</label>
                    <div id="local" class="col-sm-6">
                        <input type="text" name="local" class="form-control" value="{{$vaga->locale}}">
                    </div>

                    <label for="desc" class="col-sm-6 control-label">Descrição</label>
                    <div id="desc" class="col-sm-6">
                        <textarea type="text" name="desc" class="form-control" value="{{$vaga->description}}"></textarea>
                    </div>

                </div>



                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-6">
                        <button type="submit" class="btn btn-default btn-primary">
                            <i class="fa fa-plus"></i> Editar Vaga
                        </button>
                    </div>
                </div>
            </form>
           
        </div>
    </div>
</div>
@endsection
