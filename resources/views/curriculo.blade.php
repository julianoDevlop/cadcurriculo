@extends('layouts.app')

@section('content')
<div class="row">
    <section id="img-holder" class="col-md-6 flex-center position-ref full-height">
        @if (! empty($vaga))
            <h1><strong>{{ $vaga->title }}</strong></h1>
        @endif
    </section>
    <section class="col-md-6 flex-right position-ref full-height" style="whidth: 50%;">
        @if (! empty($vaga))
            <ul>
                <li>
                    {{ $vaga->title }}
                </li>
                <li>{{ $vaga->description }}</li>
                <li>
                    {{ $vaga->locale }}
                </li>
            </ul>
        @else
            <h4>
                Não há vagas!      
            </h4>
        @endif

    </section>
</div>

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <!-- action="{{ URL::to('/curriculo') }}" -->
                <form method="POST" action="{{action('CurriculoController@store', $vaga['id'])}}" enctype="multipart/form-data" class="form-vertical">
                 @csrf
                <label for="infop" class="curriculo-form"><strong>{{ __('Informações pessoais') }}</strong></label>

                <div id="infop" class="card-body">
                    <div class="form-group row">
                        <div class="col-md-6 offset-md-3 float-container">
                            <label class="input" for="name"><strong>{{ __('NOME COMPLETO') }}</strong></label>
                            <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" 
                                            name="name" placeholder="Digite seu nome"
                                            value="{{ old('name') }}" required>
                            
                            @if ($errors->has('name'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                            @endif
                        </div>
                        <label class='control-label'></label>
                    </div>

                    <div class="form-group row">
                        
                        <div class="col-md-6 offset-md-3 float-container">
                            <label class="input" for="email">{{ __('E-MAIL') }}</label>
                            <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}"
                                            name="email" placeholder="Digite seu email"
                                            value="{{ old('email') }}" required>

                            @if ($errors->has('email'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>
                        <label class='control-label'></label>
                    </div>

                    <div class="form-group row">
                        
                        <div class="col-md-6 offset-md-3 float-container">
                            <label class="input" for="telefone">{{ __('TELEFONE (COM DDD)') }}</label>
                            <input id="telefone" type="tel" class="form-control{{ $errors->has('telefone') ? ' is-invalid' : '' }}" name="telefone" value="{{ old('telefone') }}" required>

                            @if ($errors->has('telefone'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('telefone') }}</strong>
                                </span>
                            @endif
                        </div>
                        <label class='control-label'></label>
                    </div>

                                       
                </div>
                
                <div class="card-header curriculo-header-form"><strong>{{ __('Carta de apresentação') }}</strong></div>

                <div class="card-body">
                    <div class="form-group row">
                        <div class="col-md-6 offset-md-3">
                            <label for="apres">{{ __('CONTE SUA MOTIVAÇÃO') }}</label>
                            <textarea id="apres" type="text" class="form-control{{ $errors->has('apres') ? ' is-invalid' : '' }}"
                                                name="apres" value="{{ old('apres') }}" required autofocus></textarea>

                            @if ($errors->has('apres'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('apres') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>           
                </div>

                <div class="card-header curriculo-header-form"><strong>{{ __('Últimas perguntas') }}</strong></div>

                <div class="card-body">
                    <div class="form-group row">

                        <div class="col-md-6 offset-md-3">
                            <label for="linkedin">{{ __('URL DO SEU LINKEDIN') }}</label>
                            <input id="linkedin" type="text" class="form-control{{ $errors->has('linkedin') ? ' is-invalid' : '' }}"
                                                name="linkedin" value="{{ old('linkedin') }}" required autofocus>

                            @if ($errors->has('linkedin'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('linkedin') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group row">
                        
                        <div class="col-md-6 offset-md-3">
                            <label for="github">{{ __('URL DO SEU GITHUB') }}</label>

                            <input id="github" type="text" class="form-control{{ $errors->has('github') ? ' is-invalid' : '' }}"
                                                name="github" value="{{ old('github') }}" required>

                            @if ($errors->has('github'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('github') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group row">
                        
                        <div class="col-md-6 offset-md-3">
                            
                            <label for="ingles">{{ __('QUAL SEU NÍVEL DE INGLÊS?)') }}</label>
                            <select id="ingles" type="text" required class="form-control{{ $errors->has('ingles') ? ' is-invalid' : '' }}" name="ingles">
                                <option value="">Selecione</option>
                                <option value="Básico" >Básico</option>
                                <option value="Intermediário" >Intermediário</option>
                                <option value="Avançado" >Avançado</option>
                            </select>

                            @if ($errors->has('ingles'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('ingles') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group row">
                        
                        <div class="col-md-6 offset-md-3">
                            <label for="sal">{{ __('PRETENSÃO SALARIAL)') }}</label>
                            <input id="sal" type="text" class="form-control{{ $errors->has('sal') ? ' is-invalid' : '' }}"
                                            name="sal" value="{{ old('sal') }}" required>

                            @if ($errors->has('sal'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('sal') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                                      
                </div>

                <div class="card-header curriculo-header-form"><strong>{{ __('Anexe seu currículo em PDF ou DOC') }}</strong></div>
                
                <div class="col-md-6 offset-md-3">

                <!--<label for="file" class="btn btn-danger btn-default btn-outlined form-control{{ $errors->has('file') ? ' is-invalid' : '' }}"
                    name="arquivo" value="" required>UP</label>
                <input id="file" style="visibility:hidden;" type="file">-->

                
                <!--<input for="file" type="file" name="arquivo"
                                    class="form-control{{ $errors->has('file') ? ' is-invalid' : '' }}"
                                    required value="teste"/>-->
                <input for="file" type="file" name="arquivo" value=""
                        class="btn btn-block btn-outlined form-control{{ $errors->has('file') ? ' is-invalid' : '' }}"
                        required>Selecione um arquivo</input>
                <input id="file" type="hidden" style="dysplay: none;" value="{{ csrf_token() }}"></input>
                    <!-- <button for="file" class="btn btn-outline btn-file">{{ __('Escolher arquivo') }}
                        
                    </button>-->
                    
                    @if ($errors->has('file'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('file') }}</strong>
                        </span>
                    @endif
                </div>

                <div class="card-body">
                    <div class="form-group row mb-0">
                        <div class="col-md-6 offset-md-3">
                            <button type="submit" class="btn btn-block">
                                {{ __('ENVIAR') }}
                            </button>
                        </div>
                    </div>                    
                </div>
                
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
