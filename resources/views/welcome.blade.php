<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Sistema</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
        <link href="{{ asset('css/welcome.css') }}" rel="stylesheet" type="text/css">
        <!-- Scripts -->
        <script src="{{ asset('js/scroll.js') }}" defer></script>
        
    </head>
    <body>
        <section id="top" class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/home') }}">HOME</a>
                        <a href="{{ route('register') }}">RREGISTRAR USUÁRIO</a>
                    @else
                        <a href="{{ route('register') }}">RREGISTRAR USUÁRIO</a>
                        <a href="{{ route('login') }}" align="left">LOGIN</a>                        
                        <a href="#vagas"><button type="button" class="btn btn-primary">VAGAS ABERTAS</button></a>
                    @endauth
                </div>
            @endif

            <!--<div class="content">
                <div class="title m-b-md">
                    Laravel
                </div>

                <div class="links">
                    <a href="https://laravel.com/docs">Documentation</a>
                    <a href="https://laracasts.com">Laracasts</a>
                    <a href="https://laravel-news.com">News</a>
                    <a href="https://nova.laravel.com">Nova</a>
                    <a href="https://forge.laravel.com">Forge</a>
                    <a href="https://github.com/laravel/laravel">GitHub</a>
                </div>
            </div> -->
            
        </section>
        <section id="vagas">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-header">Vagas</div>

                        <div class="card-body">
                            @if (session('status'))
                                <div class="alert alert-success" role="alert">
                                    {{ session('status') }}
                                </div>
                            @endif
                                @if(!empty($vagas)) 
                                    <div class="panel panel-default">
                                        <div class="panel-body">
                                            <table class="table table-striped">
                                                <thead>
                                                    <th>Nome</th>
                                                    <th>Descrição</th>
                                                    <th>Localização</th>
                                                </thead>

                                                <tbody>
                                                    @foreach ($vagas as $vaga)
                                                        <tr>
                                                            <td class="table-text">
                                                                <div>{{ $vaga->title }}</div>
                                                            </td>
                                                            <td class="table-text">
                                                                <div>{{ $vaga->description }}</div>
                                                            </td>
                                                            <td class="table-text">
                                                                <div>{{ $vaga->locale }}</div>
                                                            </td>

                                                            <td>
                                                            <a href="{{action('CurriculoController@index', $vaga)}}" class="btn btn-success">CANDIDATAR-SE</a>
                                                            
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                    {{ $vagas->links() }}
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                @else
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            Não há vagas cadastradas.
                                        </div>
                                    </div>
                                @endif
                        </div>
                        

                    </div>      
                       
                </div>
            </div>
        </div>
        </section>
    </body>
</html>
